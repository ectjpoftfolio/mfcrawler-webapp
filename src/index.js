import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { persistCache } from 'apollo-cache-persist';
import { createHttpLink } from 'apollo-link-http';

import App from './components/App';
import reducers from './reducers';
import registerServiceWorker from './registerServiceWorker';

// const cache = new InMemoryCache({
//   dataIdFromObject: object => object.key || null,
// });

// persistCache({
//   cache,
//   storage: window.localStorage,
// });

// const link = createHttpLink({
//   credentials: 'same-origin',
// });

const client = new ApolloClient({
  uri: 'https://mfwatch.online/graphql',
  // cache,
});

const Root = () => (
  <ApolloProvider client={client}>
    <Provider store={createStore(reducers)}>
      <HashRouter>
        <App />
      </HashRouter>
    </Provider>
  </ApolloProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();