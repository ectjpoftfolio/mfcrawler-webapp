import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link, Redirect } from 'react-router-dom';
import { Mutation, graphql } from 'react-apollo';

import ActionForm from './ActionForm';
import loginMutation from '../mutations/Login';
import currentUserQuery from '../queries/CurrentUser';

const styles = {
  formWrapper: {
    margin: {
      top: 30,
    },
  },
  titleText: {
    margin: {
      top: 45,
    },
    fontFamily: 'Palatino',
    textAlign: 'center',
    fontSize: 35,
    color: '#4e342e',
  },
  loginText: {
    margin: {
      top: 10,
    },
    textAlign: 'center',
    fontSize: 20,
    color: '#4e342e',
  },
};

const formFields = [
  {
    name: 'email',
    type: 'text',
    placeholder: 'Email',
    icon: 'email',
  },
  {
    name: 'password',
    type: 'password',
    placeholder: 'Password',
    icon: 'lock',
  },
];

const handleValidate = ({ password, email }) => {
  const errors = {};

  if (!email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = 'Invalid email address';
  }

  if (!password) {
    errors.password = 'Required';
  } else if (password.length < 6) {
    errors.password = 'Your password must be at least 6 characters';
  }

  return errors;
};

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = { errors: [], redirectToReferrer: false };
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    } else if (this.props.data.user) {
      return <Redirect to="/" />;
    }

    const { classes } = this.props;

    return (
      <div className="row container">
        <div className={classes.titleText}>mfmonitor</div>
        <hr />
        <div className={classes.loginText}>Login</div>
        <Mutation mutation={loginMutation}>
          {(loginUser, { data, loading }) => (
            <ActionForm
              submitText="Login"
              errors={this.state.errors}
              onSubmit={(values) => {
                loginUser({ variables: values, refetchQueries: [{ query: currentUserQuery }] });
              }}
              className={classes.formWrapper}
              loading={loading || this.props.data.loading}
              validate={handleValidate.bind(this)}
              formFields={formFields}
              form="loginForm"
            />
          )}
        </Mutation>
        <div className="center-align">
          <Link to="/signup">Click Here to Sign Up</Link>
        </div>
      </div>
    );
  }
}

const LoginPageStyle = injectSheet(styles)(LoginPage);

export default graphql(currentUserQuery)(LoginPageStyle);
