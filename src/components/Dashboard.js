import React, { Component } from 'react';
import { BottomNavigation, BottomNavigationAction, Paper } from '@material-ui/core';
import { graphql } from 'react-apollo';

import currentUserQuery from '../queries/CurrentUser';
import FundList from './FundList';
import Settings from './Settings';
import SavedFunds from './SavedFunds';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = { selectedIndex: 1 };
  }

  onNavSelect = (event, value) => {
    this.setState({ selectedIndex: value });
  }

  renderContent() {
    switch (this.state.selectedIndex) {
      case 0: return <FundList />;
      case 1: return <SavedFunds user={this.props.data.user} />;
      case 2: return <Settings />;

      default: return <SavedFunds />;
    }
  }

  render() {
    const { selectedIndex } = this.state;

    return (
      <div>
        <div style={{
          overflow: 'scroll',
          bottom: 56,
          marginTop: 56,
          position: 'relative',
        }}
        >
          {this.renderContent()}
        </div>
        <Paper
          style={{
            position: 'fixed',
            left: 0,
            bottom: 0,
            width: '100%',
          }}
          zDepth={1}
        >
          <BottomNavigation
            value={selectedIndex}
            onChange={this.onNavSelect}
            showLabels
          >
            <BottomNavigationAction
              style={{}}
              label="Fund Prices"
              icon={<i className="material-icons">show_chart</i>}
            />
            <BottomNavigationAction
              label="Dashboard"
              icon={<i className="material-icons">dashboard</i>}
            />
            <BottomNavigationAction
              label="Settings"
              icon={<i className="material-icons">settings</i>}
            />
          </BottomNavigation>
        </Paper>
      </div>
    );
  }
}

export default graphql(currentUserQuery)(Dashboard);
