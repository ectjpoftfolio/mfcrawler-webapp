import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { withApollo, graphql, Mutation, compose } from 'react-apollo';
import { Link, withRouter } from 'react-router-dom';

import fundsQuery from '../queries/Funds';
import createTransactionMutation from '../mutations/CreateTransaction';
import getTransactionsQuery from '../queries/Transactions';
import currentUserQuery from '../queries/CurrentUser';
import ActionForm, { NUMBER_FORMAT_TYPE, SELECT_TYPE, DATE_TYPE } from './ActionForm';

const styles = {
  errors: {
    color: 'red',
  },
  formWrapper: {
    margin: {
      top: 30,
    },
  },
  formTitleText: {
    textAlign: 'center',
    fontSize: 18,
    color: '#4e342e',
  },
  backArrowWrapper: {
    margin: {
      top: 10,
    },
  },
};

const handleValidate = ({
  fundSelect, amount, netAmount, transactionDate, nav,
}) => {
  const errors = {};

  if (!fundSelect) {
    errors.fundSelect = 'Required';
  }

  if (!amount) {
    errors.amount = 'Required';
  } else if (amount < 0.1) {
    errors.amount = 'Not a valid positive number';
  }

  if (!netAmount) {
    errors.netAmount = 'Required';
  } else if (netAmount < 0.1) {
    errors.netAmount = 'Not a valid positive number';
  }

  if (!transactionDate) {
    errors.transactionDate = 'Required';
  } else if (!transactionDate.isValid()) {
    errors.transactionDate = 'Invalid Date';
  }

  if (!nav) {
    errors.nav = 'Required';
  } else if (nav < 0.1) {
    errors.nav = 'Not a valid positive number';
  }

  return errors;
};

class CreateTransaction extends Component {
  constructor(props) {
    super(props);

    this.state = { errors: [] };
  }

  getFormFields() {
    return [
      {
        name: 'fundSelect',
        type: SELECT_TYPE,
        selectData: {
          options: this.props.data.funds,
          keyReferrer: 'id',
          valueReferrer: 'id',
          labelReferrer: 'abbreviation',
        },
      },
      {
        name: 'nav',
        type: NUMBER_FORMAT_TYPE,
        placeholder: 'NAV',
        icon: 'timeline',
        numberFormatData: {
          decimalScale: 4,
          allowNegative: false,
        },
      },
      {
        name: 'amount',
        type: NUMBER_FORMAT_TYPE,
        placeholder: 'Amount',
        icon: 'attach_money',
        numberFormatData: {
          decimalScale: 2,
          allowNegative: false,
        },
      },
      {
        name: 'netAmount',
        type: NUMBER_FORMAT_TYPE,
        placeholder: 'Net Amount',
        icon: 'money_off',
        numberFormatData: {
          decimalScale: 2,
          allowNegative: false,
        },
      },
      {
        name: 'transactionDate',
        icon: 'date_range',
        type: DATE_TYPE,
      },
    ];
  }

  render() {
    const { loading, error } = this.props.data;
    const { classes, client } = this.props;
    const { user } = client.readQuery({ query: currentUserQuery });

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    return (
      <div className="container">
        <div className="col s12">
          <Link className={classes.backLink} to="/dashboard">
            <div className={classes.backArrowWrapper}>
              <i className="material-icons brown-text text-darken-3">arrow_back</i>
            </div>
          </Link>
        </div>
        <div className="row">
          <div className={classes.formTitleText}>Add Transaction</div>
          <Mutation
            mutation={createTransactionMutation}
            onCompleted={(data) => {
              this.props.history.push('/dashboard');
            }}
          >
            {(createTransaction, { data, loading }) => {
              return (
                <ActionForm
                  submitText="Add Transaction"
                  errors={this.state.errors}
                  onSubmit={({
                    fundSelect, transactionDate, nav, amount, netAmount,
                  }) => {
                    const parameters = {
                      fundId: fundSelect,
                      userId: String(user.id),
                      date: transactionDate.unix(),
                      nav,
                      amount,
                      netAmount,
                    };
                    createTransaction({
                      variables: parameters,
                      refetchQueries: [{
                        query: getTransactionsQuery,
                        variables: { userId: user.id },
                      }],
                    });
                  }}
                  className={classes.formWrapper}
                  loading={loading}
                  validate={handleValidate.bind(this)}
                  formFields={this.getFormFields()}
                  form="createTransactionForm"
                />
              );
            }}
          </Mutation>
        </div>
      </div>
    );
  }
}

const CreateTransactionStyle = injectSheet(styles)(CreateTransaction);

export default compose(
  withRouter,
  withApollo,
  graphql(fundsQuery),
)(CreateTransactionStyle);
