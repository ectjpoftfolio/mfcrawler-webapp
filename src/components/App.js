import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { graphql } from 'react-apollo';

import LoginPage from './LoginPage';
import SignupPage from './SignupPage';
import Dashboard from './Dashboard';
import CreateTransaction from './CreateTransaction';
import currentUserQuery from '../queries/CurrentUser';

const PrivateRoute = ({
  component: Component, user, path, ...rest
}) => (
    <Route
      {...rest}
      render={(props) => {
        if (user) {
          return String(path) === '/' ?
            <Redirect to={{ pathname: '/dashboard', state: { from: props.location } }} /> :
            <Component {...props} />;
        }
        return (<Redirect to={{ pathname: '/login', state: { from: props.location } }} />);
      }}
    />
  );

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#5D4037',
    },
    secondary: {
      main: '#795548',
    },
  },

});

const App = props => (
  <MuiThemeProvider theme={theme}>
    <Switch>
      <PrivateRoute exact user={props.data.user} path="/" />
      <PrivateRoute user={props.data.user} path="/dashboard" component={Dashboard} />
      <PrivateRoute user={props.data.user} path="/transactions/create" component={CreateTransaction} />
      <Route path="/login" component={LoginPage} />
      <Route path="/signup" component={SignupPage} />
    </Switch>
  </MuiThemeProvider>
);

export default graphql(currentUserQuery)(App);
