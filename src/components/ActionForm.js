import React from 'react';
import { Field, reduxForm } from 'redux-form';
import injectSheet from 'react-jss';
import NumberFormat from 'react-number-format';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

export const NUMBER_FORMAT_TYPE = 'NUMBER_FORMAT_TYPE';
export const SELECT_TYPE = 'SELECT_TYPE';
export const DATE_TYPE = 'DATE_TYPE';

const styles = {
  errors: {
    color: 'red',
  },
};

const inlineStyle = {
  ':focus': {
    'border-bottom': '1px solid #ffccbc',
    'box-shadow': '0 1px 0 0 #ffccbc',
  },
};

const renderField = ({
  icon, input, placeholder, type,
  meta: { touched, error, warning },
  input: { onChange, value, onBlur },
  numberFormatData,
  selectData,
}) => {

  if (type === NUMBER_FORMAT_TYPE) {
    const { decimalScale, allowNegative } = numberFormatData;

    return (
      <div>
        <div className="row valign-wrapper">
          <i className="material-icons left brown-text text-lighten-2">{icon}</i>
          <NumberFormat
            style={inlineStyle}
            placeholder={placeholder}
            value={value}
            onValueChange={(values) => { onChange(values.value); }}
            onBlur={onBlur}
            decimalScale={decimalScale}
            allowNegative={allowNegative}
          />
        </div>
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  } else if (type === SELECT_TYPE) {
    const {
      options, keyReferrer, valueReferrer, labelReferrer,
    } = selectData;

    return (
      <div>
        <div>
          <select onChange={(e) => { onChange(e.target.value); }} className="browser-default">
            {
              (options ? options.map((option) => {
                return <option key={option[keyReferrer]} value={option[valueReferrer]}>{option[labelReferrer]}</option>;
              }) : <option key="loading.." value="">Loading..</option>)
            }
          </select>
        </div>
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  } else if (type === DATE_TYPE) {
    return (
      <div>
        <div className="row valign-wrapper">
          <i className="material-icons left brown-text text-lighten-2">{icon}</i>
          <DatePicker
            style={inlineStyle}
            className="left"
            selected={typeof (value) !== 'object' ? moment() : value}
            onChange={(date) => { onChange(date); }}
          />
        </div>
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  }

  return (
    <div>
      <i className="material-icons prefix brown-text text-lighten-2">{icon}</i>
      <input {...input} style={inlineStyle} placeholder={placeholder} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  );
};

const ActionForm = (props) => {
  const {
    handleSubmit, classes, submitText, loading, formFields,
  } = props;

  return (
    <form onSubmit={handleSubmit}>
      {
        formFields.map(formField => (
          <div key={formField.name} className="row">
            <div className="input-field">
              <Field
                name={formField.name}
                component={renderField}
                type={formField.type}
                placeholder={(formField.placeholder ? formField.placeholder : undefined)}
                icon={formField.icon}
                selectData={formField.selectData ? formField.selectData : undefined}
                numberFormatData={formField.numberFormatData ? formField.numberFormatData : undefined}
              />
            </div>
          </div>
        ))
      }
      <div className={classes.errors}>
        {props.errors.map(error => <div key={error}>{error}</div>)}
      </div>
      <div className="row center-align">
        <button
          className="btn waves-light white-text valign-wrapper"
          type="submit"
          name="action"
          disabled={loading}
        >{submitText}
          {loading ? <i className="fas fa-spinner fa-spin right" style={{ fontSize: 24 }} /> : <i className="material-icons right">send</i>}
        </button>
      </div>
    </form>
  );
};

const ActionFormStyle = injectSheet(styles)(ActionForm);

// all settings are from parent
const ActionFormRedux = reduxForm({})(ActionFormStyle);

export default ActionFormRedux;
