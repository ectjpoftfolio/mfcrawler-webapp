import React from 'react';
import { graphql } from 'react-apollo';
import { Line } from 'react-chartjs-2';

import fundsQuery from '../queries/Funds';

const getDate = (dateString) => {
  const stringParts = dateString.split('/');
  return new Date(stringParts[2], stringParts[1], stringParts[0]);
};

const getChartData = ({ name, abbreviation, prices }) => {
  const yAxisData = [];

  // sort prices according to date first
  const sortedPrices = prices.slice().sort((previous, next) => {
    const previousDate = getDate(previous.date);
    const nextDate = getDate(next.date);

    return previousDate - nextDate;
  });

  const firstDate = sortedPrices.map(sortedPrice => sortedPrice.date)
    .reduce((min, current) => {
      const minDate = getDate(min);
      const currentDate = getDate(current);
      return (currentDate < minDate) ? current : min;
    }, '99/99/9999');

  const lastDate = sortedPrices.map(sortedPrice => sortedPrice.date)
    .reduce((max, current) => {
      const maxDate = getDate(max);
      const currentDate = getDate(current);
      return (currentDate > maxDate) ? current : max;
    }, '01/01/0001');

  sortedPrices.map(({ nav }) => yAxisData.push(nav));

  return {
    labels: [firstDate, lastDate],
    datasets: [
      {
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 3,
        pointHitRadius: 10,
        data: yAxisData,
      },
    ],
  };
};

const getChartOptions = {
  legend: {
    display: false,
  },
  elements: {
    line: {
      tension: 0, // disables bezier curves
    },
  },
  animation: {
    duration: 0, // general animation time
  },
  hover: {
    animationDuration: 0, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 0, // animation duration after a resize
};

const renderFunds = funds =>
  funds.map(fund => (
    <li key={fund.id} className="collection-item">
      <span className="title center-align">{`${fund.name}(${fund.abbreviation})`}</span>
      <div width="75%" height="50%">
        <Line data={getChartData(fund)} options={getChartOptions} />
      </div>
    </li>
  ));

const FundList = (props) => {
  const { loading, funds } = props.data;

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <ul className="collection">{renderFunds(funds)}</ul>
    </div>
  );
};

export default graphql(fundsQuery)(FundList);
