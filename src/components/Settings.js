import React from 'react';
import { Mutation } from 'react-apollo';

import currentUserQuery from '../queries/CurrentUser';
import logoutMutation from '../mutations/Logout';

// const onLogoutClick = (props) => {
//   console.log(props);
//   props.mutate({
//     refetchQueries: [{ query: currentUserQuery }],
//   });
// };

const Settings = () => (
  <div className="row">
    <div className="center-align">
      <Mutation mutation={logoutMutation}>
        {(logout, { loading, data }) => (
          <button
            className="btn waves-light"
            onClick={() => logout({ refetchQueries: [{ query: currentUserQuery }] })}
            style={{ marginTop: 80 }}
            type="button"
            disabled={!!loading}
          >Logout
            {
              loading ?
                <i className="fas fa-spinner fa-spin right" style={{ fontSize: 24 }} />
                : <i className="material-icons right">exit_to_app</i>
            }
          </button>
        )}
      </Mutation>
    </div>
  </div>
);


export default Settings;
