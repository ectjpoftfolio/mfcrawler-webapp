import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link, Redirect } from 'react-router-dom';
import { graphql, Mutation } from 'react-apollo';

import ActionForm from './ActionForm';
import signupMutation from '../mutations/Signup';
import currentUserQuery from '../queries/CurrentUser';

const styles = {
  formWrapper: {
    margin: {
      top: 30,
    },
  },
  signupText: {
    margin: {
      top: 40,
    },
    textAlign: 'center',
    fontSize: 20,
    color: '#4e342e',
  },
  backArrowWrapper: {
    margin: {
      top: 10,
    },
  },
};

const formFields = [
  {
    name: 'email',
    type: 'text',
    placeholder: 'Email',
    icon: 'email',
  },
  {
    name: 'password',
    type: 'password',
    placeholder: 'Password',
    icon: 'lock',
  },
  {
    name: 'passwordRepeat',
    type: 'password',
    placeholder: 'Repeat Password',
    icon: 'lock',
  },
];


const handleValidate = ({ password, email, passwordRepeat }) => {
  const errors = {};

  if (!email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = 'Invalid email address';
  }

  if (!password) {
    errors.password = 'Required';
  } else if (password.length < 6) {
    errors.password = 'Your password must be at least 6 characters';
  }

  if (!passwordRepeat) {
    errors.passwordRepeat = 'Required';
  } else if (password !== passwordRepeat) {
    errors.passwordRepeat = 'Your passwords does not match';
  }

  return errors;
};

class SignupPage extends Component {
  constructor(props) {
    super(props);

    this.state = { errors: [], redirectToReferrer: false };
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    } else if (this.props.data.user) {
      return <Redirect to="/" />;
    }

    const { classes } = this.props;

    return (
      <div>
        <div className="row container">
          <Link className={classes.backLink} to="/">
            <div className={classes.backArrowWrapper}>
              <i className="material-icons brown-text text-darken-3">arrow_back</i>
            </div>
          </Link>
        </div>
        <div className="row">
          <div className={classes.signupText}>Sign Up</div>
          <Mutation mutation={signupMutation}>
            {(signupUser, { data, loading }) => (
              <ActionForm
                submitText="SIGNUP"
                errors={this.state.errors}
                onSubmit={(values) => {
                  signupUser({ variables: values, refetchQueries: [{ query: currentUserQuery }] });
                }}
                className={classes.formWrapper}
                loading={loading || this.props.data.loading}
                validate={handleValidate.bind(this)}
                formFields={formFields}
                form="signupForm"
              />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const SignupPageStyle = injectSheet(styles)(SignupPage);

export default graphql(currentUserQuery)(SignupPageStyle);

