import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { initTranForm } from '../actions/index';

const initData = {
  transactionDate: moment(),
};

const initialValuesWrapper = (WrappedComponent) => {
  return class extends Component {
    constructor(props) {
      super(props);

      this.props.initFormInitialState(initData);
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
};


const InitializeFormState = connect(
  state => ({
    initialValues: state.transactionInit.data,
  }),
  { initFormInitialState: initTranForm },
)(initialValuesWrapper);

export default InitializeFormState;
