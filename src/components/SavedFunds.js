import React from 'react';
import { Query, withApollo } from 'react-apollo';
import _ from 'lodash';
import { Link } from 'react-router-dom';

import getTransactionsQuery from '../queries/Transactions';
import currentUserQuery from '../queries/CurrentUser';

const shift = (number, precision, reverseShift) => {
  if (reverseShift) {
    precision = -precision;
  }
  const numArray = ("" + number).split("e");
  return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
};

const round = (number, precision) => {
  return shift(Math.round(shift(number, precision, false)), precision, true);
};

const getHeaderNav = (navDiff) => {
  if (navDiff) {
    return (
      <div className="valign-wrapper">
        {`( ${round(navDiff, 4)}`}
        {(navDiff > 0 ?
          <i
            style={{ color: '#1b5e20', fontSize: 35 }}
            className="material-icons"
          >arrow_drop_up
          </i> :
          <i
            className="material-icons"
            style={{ color: '#d50000', fontSize: 35 }}
          >arrow_drop_down
          </i>
        )}
        {')'}
      </div>
    );
  }
  return '';
};

const renderTransactions = (transactions) => {

  const groupedTransactions = _.groupBy(transactions, (transaction) => {
    return transaction.fund.id;
  });

  return _.map(groupedTransactions, (value) => {
    const fundTitle = `${value[0].fund.abbreviation}`;
    const twoOldestFund = value[0].fund.prices.slice(-2);

    const latestFundInfo = twoOldestFund.pop();
    const latestNav = latestFundInfo.nav;
    const latestDate = latestFundInfo.date;

    const previousDayFundInfo = twoOldestFund.pop();

    let navDiff = null;
    if (previousDayFundInfo) {
      const previousDayNav = previousDayFundInfo.nav;
      navDiff = latestNav - previousDayNav;
    }

    let total = 0;
    let gain = 0;
    value.map(({ id, date, amount, netAmount, nav }) => {
      total += amount;
      gain += (netAmount / nav * latestNav) - netAmount - (amount - netAmount);
    });

    return (
      <li key={value[0].id} className="collection-item">
        <div>
          <h6 className="valign-wrapper center-align">
            {`${fundTitle} `}{getHeaderNav(navDiff)}
          </h6>
        </div>
        <table className="highlight">
          <thead>
            <tr>
              <th>NAV Date</th>
              <th>Total</th>
              <th>Returns</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{latestDate}</td>
              <td>{`RM: ${round(total, 2)} `}</td>
              <td>{`RM: ${round(gain, 2)} `}</td>
            </tr>
          </tbody>
        </table>
      </li>
    );
  });
};

const SavedFunds = (props) => {
  const { client } = props;
  const { user } = client.readQuery({ query: currentUserQuery });

  return (
    <div>
      <div style={{ marginTop: 10, marginRight: 10 }} className="right-align">
        <Link to="/transactions/create">
          <button className="btn-small waves-light right-align brown lighten-1" type="button" name="add-transaction">Add
            <i className="material-icons right">add</i>
          </button>
        </Link>
      </div>
      <Query query={getTransactionsQuery} variables={{ userId: String(user.id) }} >
        {({ loading, error, data }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>`Error! ${error.message} `</div>;

          return (
            <ul className="collection">
              {renderTransactions(data.transactions)}
            </ul>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(SavedFunds);
