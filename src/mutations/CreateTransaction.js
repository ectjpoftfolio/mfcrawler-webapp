import gql from 'graphql-tag';

export default gql`
mutation creatTransaction($fundId: ID!, $userId: ID!, $date:Int!, $nav: Float!, $amount: Float!, $netAmount: Float!){
  createTransaction(fundId: $fundId, userId: $userId, date:$date, nav:$nav, amount:$amount, netAmount:$netAmount){
   	id
    nav
    date
    amount
    netAmount
    fund{
      id
      prices{
        nav
      }
    }
  }
}
`;
