
export const INIT_TRAN_FORM = 'INIT_TRAN_FORM';

export function initTranForm(data) {
  return {
    type: INIT_TRAN_FORM,
    payload: data,
  };
}
