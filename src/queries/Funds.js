import gql from 'graphql-tag';

export default gql`
{
  funds{
    id
    name
    abbreviation
    prices{
      id
      nav
      date
    }
  }
}
`;
