import gql from 'graphql-tag';

export default gql`
query Transactions($userId: String!){
  transactions(userId: $userId){
    id
    amount
    netAmount
    nav
    date
    fund{
      name
      abbreviation
      id
      prices{
        id
        nav
        date
      }
    }
  }
}
`;
