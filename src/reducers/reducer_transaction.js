import { INIT_TRAN_FORM } from '../actions/index';

export default function (state = [], action) {
  switch (action.type) {
    case INIT_TRAN_FORM:
      return {
        data: action.payload,
      };
    default: return state;
  }
}
