import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import TransactionReducer from './reducer_transaction';

const rootReducer = combineReducers({
  form: formReducer,
  transactionInit: TransactionReducer,
});

export default rootReducer;
